For the following task :

1) Following 3 tables have been generated in MySQL, along with some sample data.

2) Query to perform following task is :
	select employee.first_name, employee.last_name, manager.last_name from table_1 INNER JOIN manager ON table_1.id_manager = manager.id_manager INNER JOIN employee ON table_1.id_employee = employee.id_employee where table_1.dt_work_from < '2020-01-31' or table_1.dt_work_to > '2020-01-01';

3) a backup of these tables along with whole database named "internAssignments" have been dumped in .sql file using "mysqldump" for reference.
